package goweb

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func HelloHandler(writer http.ResponseWriter, request *http.Request) {
	fmt.Println(writer, "Hello Nurdin")
}

func TestHttp(t *testing.T) {
	request := httptest.NewRequest(http.MethodGet, "http://localhost/hi", nil)
	recorder := httptest.NewRecorder()
	HelloHandler(recorder, request)
}
